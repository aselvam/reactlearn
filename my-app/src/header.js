import React, { Component } from 'react';
import ReactDOM from 'react-dom';


class Header extends Component {
  render(){
    const title = this.props.title
    return(
      <h1>Header {title} </h1>
    )
  }
}

export default Header;
