import React, { Component } from  'react';

class Input extends Component {
   constructor(props){
     super(props)
     this.state = {
        'name':''
     }
   }

handleChange(e){
  this.setState(
    {
      'name':e.target.value.toUpperCase()
    }
   );
}

   render(){
     return (
       <input value={this.state.name} onChange={this.handleChange.bind(this)}/>
     )
   }
}

export default Input;
