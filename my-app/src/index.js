import React from 'react';
import ReactDOM from 'react-dom';
//import App from './App';
import Header from './header';
import Footer from './footer';
import Navigation from './navigation';
import PageAdmin from './pageadmin';
import Input from './input';
import registerServiceWorker from './registerServiceWorker';
import './index.css';

let menuList = [
  {name: 'Dashboard' ,id:'1'},
  {name: 'Settings' ,id:'2'},
  {name: 'ContactUs' ,id:'3'}
]

class Backend {
constructor(){

}
 getAllPages(){
    var pages=[
      {id:'1', name:'Introduction'},
      {id:'2', name:'Home'}
    ]
    return pages
  }
}

let backend = new Backend()

ReactDOM.render(
  <div>
    <Header title="Welcome"/>
    <Navigation menus={menuList} />
    <Input />
    <PageAdmin backend= {backend} />
    <Footer />
  </div>,
   document.getElementById('root')
 );
registerServiceWorker();
