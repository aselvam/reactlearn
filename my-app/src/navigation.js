import React, { Component } from 'react';
import ReactDOM from 'react-dom';

class Navigation extends Component{
  render(){
    let elems = this.props.menus.map((ele) => {
      return (
          <li key={ele.id}>  {ele.name} </li>
      )
    });
    return (
        <ul> {elems} </ul>
    )
  }
}

export default Navigation;
