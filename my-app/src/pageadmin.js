import React, { Component } from 'react';
import Page from './page';

class PageAdmin extends Component {

  constructor(props){
    super(props)
    this.state = {
      "pages" : []
    }
  }

  componentWillMount(){
    console.log("props " + this.props.backend)
    this.setState({
      "pages" : this.props.backend.getAllPages()
    })
  }

  render(){
    let pgs = this.state.pages.map(function(p)  {
      return(
            <li key={p.id}> <Page {...p} /> </li>
          )
          })
    return( <ol>{ pgs }</ol>)


  }
}


export default PageAdmin;
