import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  render() {
    return (
      <h1>  hello </h1>
    );
  }
}

class Header extends Component {
  render(){
    return(
      <h1>Header </h1>
    )
  }
}

class Content extends Component {

   render() {
      return (
         <div>
            <h2>Content</h2>
            <p>This is the content text { 3 + 2} </p>
         </div>
      );
   }
}

export default App;
//export default Header;
