import React, { Component } from 'react';
import ReactDOM from 'react-dom';

class Footer extends Component {

   render() {
      return (
         <div>
            <h2>Footer</h2>
            <p>This is the footer text { 3 + 2} </p>
         </div>
      );
   }
}

export default Footer;
